# MaChAmp at SemEval-2022 tasks 2, 3, 4, 6, 10, 11, and 12: Multi-task Multi-lingual Learning for a Pre-selected Set of Semantic Datasets

This repository contains the code to replicate our experiments for our participation paper for SemEval 2022. 

##  Implementation
All code necessary to rerun the experiments can be found in the scripts folder. Configuration files for MaChAmp are found in the configs folder. Unfortunately not all data is publicly available, and the code in scripts/0.prep\* should probably be ran manually. 


We use the approach described in detail [here](https://robvanderg.github.io/blog/repro.htm) for reproducability, which means that all commands to redo the experiments can be found in scripts/runAll.sh and all code to generate tables/graphs can be found in scripts/genAll.sh.


