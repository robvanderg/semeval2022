inPath = 'data/task4/dontpatronizeme/dontpatronizeme_v1.4/dontpatronizeme_pcl.tsv'
outPath = inPath + '.converted'

outFile = open(outPath, 'w')
for line in open(inPath):
    tok = line.strip().split('\t')
    if tok[-1] in ['0', '1']:
        tok[-1] = '0'
    else:
        tok[-1] = '1'
    outFile.write('\t'.join(tok) + '\n')
outFile.close()

def write(inPath, ratio, taskNumber):
    data = open(inPath).readlines()
    split = int(ratio*len(data))
    trainOut = open('data/task4/' + str(taskNumber) + '.train.en.conll', 'w')
    for line in data[:split]:
        trainOut.write(line)
    trainOut.close()
    devOut = open('data/task4/' + str(taskNumber) + '.dev.conll', 'w')
    for line in data[split:]:
        devOut.write(line)
    devOut.close()
    

write(outPath, .8, 1)
write('data/task4/dontpatronizeme/dontpatronizeme_v1.4/dontpatronizeme_categories.tsv', .8, 2)



