cut -f 7 data/task2/SemEval_2022_Task2-idiomaticity/SubTaskA/TestData/test.csv-multitask.rembert.emb.task2-a1 > pred
tail -n 2342 data/task2/SemEval_2022_Task2-idiomaticity/SubTaskA/TestData/test_submission_format.csv > bottom
head -2343 data/task2/SemEval_2022_Task2-idiomaticity/SubTaskA/TestData/test_submission_format.csv > top
sed -e 's/\r$//' -i bottom
paste bottom pred -d '' > bottom-done
mkdir -p test/task2
mkdir -p test/task2/multi
mkdir -p test/task2/multi/submission
cat top bottom-done > test/task2/multi/submission/task2_subtaska.csv
cd test/task2/multi
zip submission.zip submission/task2_subtaska.csv
cd ../../../

cut -f 7 data/task2/SemEval_2022_Task2-idiomaticity/SubTaskA/TestData/test.csv-task2-a1 > pred
mkdir -p test/task2/single/submission
paste bottom pred -d '' > bottom-done
mkdir -p test/task2/single
mkdir -p test/task2/single/submission
cat top bottom-done > test/task2/single/submission/task2_subtaska.csv
cd test/task2/single
zip submission.zip submission/task2_subtaska.csv
cd ../../../

