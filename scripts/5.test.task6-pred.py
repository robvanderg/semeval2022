import myutils
import os
import csv
import pandas

import myutils

os.system('mkdir -p test/task6')

def cleanFile(path):
    outPath = open(path + '.cleaned', 'w')
    data = pandas.read_csv(path)['text']
    for item in data:
        outPath.write('_\t' + item.replace('\n', ' ') + '\n')
    outPath.close()
    return '../' + path + '.cleaned'

def cleanFile2(path):
    outPath = open(path + '.cleaned', 'w')
    data0 = pandas.read_csv(path)['text_0']
    data1 = pandas.read_csv(path)['text_1']
    for item0, item1 in zip(data0, data1):
        outPath.write('_' + '\t' + item0.replace('\n', ' ') + '\t_\t' + item1.replace('\n', '') + '\n')
    outPath.close()
    return '../' + path + '.cleaned'
    
# predict
for modelName in ['task6-a', 'multitask.rembert.emb.task6-a']:
    model = myutils.getModel(modelName)
    for tgtFile in ['taskA.En.input.csv', 'task_A_AR_test.csv']:
        inPath = cleanFile('data/task6/test/' + tgtFile)
        cmd = 'python3 predict.py ' + model[4:] + ' ' + inPath + ' ' + inPath + '.' + modelName
        print(cmd)

for modelName in ['task6-b', 'multitask.rembert.emb.task6-b']:
    model = myutils.getModel(modelName)
    print(model, modelName)
    for tgtFile in ['taskB.En.input.csv']:
        inPath = cleanFile('data/task6/test/' + tgtFile)
        cmd = 'python3 predict.py ' + model[4:] + ' ' + inPath + ' ' + inPath + '.' + modelName
        print(cmd)

for modelName in ['task6-c', 'multitask.rembert.emb.task6-c']:
    model = myutils.getModel(modelName)
    for tgtFile in ['taskC.En.input.csv', 'task_C_AR_test.csv']:
        inPath = cleanFile2('data/task6/test/' + tgtFile)
        cmd = 'python3 predict.py ' + model[4:] + ' ' + inPath + ' ' + inPath + '.' + modelName
        print(cmd)


