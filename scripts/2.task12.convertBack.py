# convert from conll format to the format of SemEval2022 task12
# INPUT:
## id: 1802.07164v1.Cubic_graphs_their_Ehrhart_quasi_polynomials_and_a_scissors_congruence_phenomenon/paragraph_48
## phase: dev
## topic: math.co
## document: 1802.07164v1.Cubic_graphs_their_Ehrhart_quasi_polynomials_and_a_scissors_congruence_phenomenon
## paragraph: paragraph_48
## prefix: selected/dev/math.co-ann7/1802.07164v1.Cubic_graphs_their_Ehrhart_quasi_polynomials_and_a_scissors_congruence_phenomenon/paragraph_48
#1       Let     O       _       whitespaceBefore=0
#2       $       O       _       whitespaceBefore=1
#3       W       B-SYMBOL        _       whitespaceBefore=1
#4       _       I-SYMBOL        _       whitespaceBefore=1
#5       0       I-SYMBOL        _       whitespaceBefore=1
#6       $       O       _       whitespaceBefore=1
#7       be      O       _       whitespaceBefore=1
#8       the     O       _       whitespaceBefore=1
#9       trail   B-PRIMARY       -1:Direct       whitespaceBefore=1
#10      used    O       _       whitespaceBefore=1
#11      in      O       _       whitespaceBefore=1
#12      the     O       _       whitespaceBefore=1
#13      first   O       _       whitespaceBefore=1
#14      NNI     O       _       whitespaceBefore=1
#15      move    O       _       whitespaceBefore=1

# OUTPUT
#{
#  "1802.07164v1.Cubic_graphs_their_Ehrhart_quasi_polynomials_and_a_scissors_congruence_phenomenon/paragraph_48": {
#    "id": "1802.07164v1.Cubic_graphs_their_Ehrhart_quasi_polynomials_and_a_scissors_congruence_phenomenon/paragraph_48",
#    "phase": "dev",
#    "topic": "math.co",
#    "document": "1802.07164v1.Cubic_graphs_their_Ehrhart_quasi_polynomials_and_a_scissors_congruence_phenomenon",
#    "paragraph": "paragraph_48",
#    "prefix": "selected/dev/math.co-ann7/1802.07164v1.Cubic_graphs_their_Ehrhart_quasi_polynomials_and_a_scissors_congruence_phenomenon/paragraph_48",
#    "text": "Let $W_0$ be the trail used in the first NNI move 
#    "entity": {
#      "T1": {
#        "eid": "T1",
#        "label": "PRIMARY",
#        "start": 17,
#        "end": 22,
#        "text": "trail"
#      },
#      "T2": {
#        "eid": "T2",
#        "label": "SYMBOL",
#        "start": 5,
#        "end": 8,
#        "text": "W_0"
#      },
#    "relation": {
#      "R1": {
#        "rid": "R1",
#        "label": "Direct",
#        "arg0": "T1",
#        "arg1": "T2"
#      }
#    }
#  }
#}

import sys
import json
import pprint

def readConll(path):
    data = []
    meta = []
    instance = []
    for line in open(path):
        if len(line) <= 2:
            data.append((instance, meta))
            instance = []
            meta = []
        else:
            if line[0] == '#':
                meta.append(line.strip())
            else:
                instance.append(line.strip().split('\t'))
    return data
        

def convText(instance):
    ents = {}
    rels = {}
    entNum = 1
    text = ''
    entitiesList = []
    for wordIdx, word in enumerate(instance):
        entitiesList.append([])
        numSpaces = int(word[-1].split('=')[-1])
        for label in word[2].split('|'):
            if label[0] == 'B':
                label = word[2].split('|')[0][2:]
                begCharIdx = len(text) + numSpaces
                endCharIdx = len(text) + numSpaces + len(word[1])
                labelText = word[1]
                for endWordIdx in range(wordIdx+1,len(instance)):
                    if 'I-' + label not in instance[endWordIdx][2]:
                        break
                    else:
                        iSpaces = int(instance[endWordIdx][-1].split('=')[-1])
                        endCharIdx += len(instance[endWordIdx][2]) + iSpaces
                        labelText += ' ' * iSpaces + instance[endWordIdx][1]
                endCharIdx = begCharIdx + len(labelText)
                ents['T' + str(entNum)] = {'eid': 'T' + str(entNum), 'start': begCharIdx, 'end': endCharIdx, 'label': label, 'text': labelText}
                entitiesList[-1].append('T' + str(entNum))
                entNum += 1
        text += ' ' * numSpaces + word[1]

    relNumber = 1
    for wordIdx, word in enumerate(instance):
        for relation in word[3].split('|'):
            if relation == '_':
                continue
            loc, label = relation.split(':')
            dist = int(loc[1:])
            searchDist = 0
            targetT = None
            if loc[0] == '+':
                for i in range(wordIdx, len(instance)):
                    searchDist += len(entitiesList[i])
                    if searchDist >= dist:
                        targetT = entitiesList[i][0]
                        break
            else:
                for i in reversed(range(0, wordIdx)):
                    searchDist += len(entitiesList[i])
                    if searchDist >= dist:
                        targetT = entitiesList[i][0]
                        break
            # predicted relation, but no entity, error: skip
            if len(entitiesList[wordIdx]) == 0:
                continue
            # if targetT doesnt exist (last entity, looks for next entity) skip
            if targetT == None:
                continue
            srcT = entitiesList[wordIdx][0]
            rels['R' + str(relNumber)] = {'rid': 'R'+ str(relNumber), 'label': label, 'arg0':srcT, 'arg1': targetT}
            relNumber += 1
                

    return text, ents, rels
    

data = {}
for path in sys.argv[1:]:
    for instance, meta in readConll(path):
        instanceDict = {}
        for item in meta:
            instanceDict[item.strip().split(':')[0][2:]] = item[item.find(':')+1:].strip()

        text, ents, rels = convText(instance)
        instanceDict['text'] = text
        instanceDict['entity'] = ents
        instanceDict['relation'] = rels
        #pprint.pprint(instanceDict)
        
        data[instanceDict['id']] = instanceDict
    open(path + '.converted', 'w').write(json.dumps(data))

