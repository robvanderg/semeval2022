import pprint
import json
import os
import sys

# convert from conll to json, like follows:
#1       I       O       O       _
#2       can     O       O       _
#3       ’t      O       O       _
#4       explain O       O       _
#5       in      O       O       _
#6       words   O       O       _
#7       how     B-expression    O       Positive:null:+1
#8       grand   I-expression    O       _
#9       this    I-expression    B-nominal       _
#10      place   I-expression    I-nominal       _
#11      looks   I-expression    O       _
#12      .       O       O       _
#
#{'opinions': [{'Intensity': 'Standard',
#               'Polar_expression': [['how grand looks'], ['26:52']],
#               'Polarity': 'Positive',
#               'Source': [[], []],
#               'Target': [['this place'], ['36:46']]}],
# 'sent_id': '../opener/en/kaf/hotel/english00192_e3fe22eeb360723a699504a27e13065e-5',
# 'text': 'I can ’t explain in words how grand this place looks .'}

def readConll(path):
    data = []
    curSent = []
    for line in open(path):
        if line.startswith('# sent_id'):
            curSent.append(line[10:-1])
        if line[0] == '#':
            continue
        elif len(line) > 2:
            curSent.append(line.strip().split('\t'))
        else:
            data.append(curSent)
            curSent = []
    return data


def getBIO(data, text, startIdx, columnIdx, direction=None):
    if direction == 0:
        begIdx = startIdx
    elif direction < 0:
        for begIdx in reversed(range(0, startIdx)):
            if 'B' in data[begIdx][columnIdx]:
                direction+=1
                if direction == 0:
                    break
    elif direction > 0:
        for begIdx in (range(startIdx, len(data))):
            if 'B' in data[begIdx][columnIdx]:
                direction-=1
                if direction == 0:
                    break

    ## gets the longest matching span for now:
    if begIdx+1 == len(data):
        endIdx = len(data)
    for endIdx in range(begIdx+1, len(data)):
        if 'I' not in data[endIdx][columnIdx]:
            break
    if endIdx +1 == len(data):
        if 'I' in data[-1][columnIdx]:
            endIdx += 1
    textTok = text.split(' ')
    charBeg = sum([len(x) for x in textTok[:begIdx-1]]) + begIdx -1
    charEnd = sum([len(x) for x in textTok[:endIdx-1]]) + endIdx -2
    return [text[charBeg:charEnd].strip(), [str(charBeg) + ':' + str(charEnd)]]

def convert(srcPath, tgtPath):
    srcData = readConll(srcPath)
    tgtData = []
    for instance in srcData:
        output = {}
        text = [x[1] for x in instance[1:]]
        output['text'] = ' '.join(text)
        output['sent_id'] = instance[0]
        #if 'english00200_e8f707795fc0c7f605a1f7115c3da711-8' not in instance[0]:
        #    continue
        output['opinions'] = []
        for wordIdx in range(1, len(instance)):
            if instance[wordIdx][4] != '_':
                polarity = instance[wordIdx][4].split(':')[0]
                sourceRelPos = instance[wordIdx][4].split(':')[1]
                targetRelPos = instance[wordIdx][4].split(':')[2].split('|')[0]
                source = [[], []]
                if sourceRelPos != 'null':
                    source = getBIO(instance, output['text'], wordIdx, 3, int(sourceRelPos))
                target = [[], []]
                if targetRelPos != 'null':
                    target = getBIO(instance, output['text'], wordIdx, 3, int(targetRelPos))
                expression = getBIO(instance, output['text'], wordIdx, 2, 0)
                annotation = {"Source": source, "Target": target, "Polar_expression": expression, "Polarity": polarity}
                output['opinions'].append(annotation)
        tgtData.append(output)
        #pprint.pprint(output)
    json.dump(tgtData, open(tgtPath, 'w'))

#if len(sys.argv) == 1:
#    for datafile in os.listdir('data/task10/'):
#        if datafile.endswith('conll') and (datafile.startswith('dev') or datafile.startswith('train')):
#            convert('data/task10/' + datafile, 'data/task10/' + datafile + '.converted')
            #dataset = datafile.split('.')[1]
            #split = datafile.split('.')[0]
            #cmd = 'python3 data/task10/semeval22_structured_sentiment/evaluation/evaluate_single_dataset.py data/task10/semeval22_structured_sentiment/data/' + dataset + '/' + split + '.json ' + 'data/task10/' + datafile + '.converted'
            #print(datafile)
            #os.system(cmd)

#else:
#    for path in sys.argv[1:]:
#        convert(path, path + '.converted')
for path in sys.argv[1:]:
    convert(path, path + '.converted')

