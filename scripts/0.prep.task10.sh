cd data
mkdir task10
cd task10
git clone https://github.com/jerbarnes/semeval22_structured_sentiment.git
cd semeval22_structured_sentiment
git reset --hard 212477ef7beb9c6d9842984059a3e649050a74c4

cd data
cp ~/Downloads/mpqa_2_0_database.tar.gz mpqa
cd mpqa
# install and download stanza models
# pip3 install stanza
# python3
# import stanza
# stanza.download()
bash process_mpqa.sh
cd ../

cp ~/Downloads/DarmstadtServiceReviewCorpus.zip darmstadt_unis
cd darmstadt_unis
bash process_darmstadt.sh
cd ../../../../

python3 scripts/0.prep.task10.py

