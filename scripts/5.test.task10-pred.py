import myutils
import os
import json

def getWordIdxs(text, loc):
    if loc == [[], []]:
        return -1,-1
    loc = [int(x) for x in loc[1][0].split(':')]
    beg = text[:loc[0]].count(' ')
    end = text[:loc[1]].count(' ')+1
    return beg, end

def getRelPos(startPosition, tgtPosition, listEntities):
    distance = 0
    counter = 0
    if tgtPosition == -1:
        return 'null'

    #print(startPosition, tgtPosition)
    #print(listEntities)
    if startPosition < tgtPosition:
        direction = '+'
        relevantRange = list(range(startPosition, len(listEntities)))
    elif startPosition == tgtPosition:
        return '+0'
    else:
        direction = '-'
        relevantRange = reversed(range(0, startPosition))

    for loopIdx in relevantRange:
        if listEntities[loopIdx][0] == 'B':
            counter += 1
        if loopIdx == tgtPosition:
            return direction + str(counter)
    return 'ERROR'

# This checks if there are overlapping spans
def addBIO(nominals, newBeg, newEnd, labelType):
    if 'B' in nominals[newBeg] and (newBeg+1 == newEnd or 'I' in nominals[newEnd-1]):
        return nominals
    for idx in range(newBeg, newEnd):
        label = 'B-' if idx == newBeg else 'I-'
        label += labelType
        if nominals[idx] == 'O':
            nominals[idx] = label
        else:
            nominals[idx] += '|' + label
    return nominals

#langs = {'darmstadt_unis': 'en', 'mpqa': 'en', 'multibooked_ca': 'ca', 'multibooked_eu': 'eu', 'norec': 'no', 'opener_en': 'en', 'opener_es': 'es'}

def convert(path, outPath):
    data = json.load(open(path))
    outFile = open(outPath, 'w')

    for instance in data:
        #if instance['sent_id'] != '../opener/en/kaf/hotel/english00221_fbcfafad33cef47cd341ecc409a0f76a-4':
        #    continue
        words = instance['text'].split(' ')
        expressions = ['O'] * len(words)
        nominals = ['O'] * len(words)
        annotations = [ [] for _ in range(len(words)) ]
        if 'opinions' in instance:
            for annotation in instance['opinions']:
                #outFile.write('# ' + str(annotation) + '\n')
                #print('# ' + str(annotation))
                begIdx, endIdx = getWordIdxs(instance['text'], annotation['Polar_expression'])
                polarity = annotation['Polarity']
                intensity = annotation['Intensity']
                if intensity in [None,'Standard']:
                    intensity = 'Average'
                # intensity is not included for now, as it is not used in the shared task:
                # https://groups.google.com/g/structured-sent-participants/c/aIC9lg1W9eE/m/igec4zYMEgAJ?utm_medium=email&utm_source=footer

                expressions = addBIO(expressions, begIdx, endIdx, 'expression')

                label = [begIdx, polarity]

                sourceBeg, sourceEnd = getWordIdxs(instance['text'], annotation['Source'])
                nominals = addBIO(nominals, sourceBeg, sourceEnd, 'nominal')
                targetBeg, targetEnd = getWordIdxs(instance['text'], annotation['Target'])
                nominals = addBIO(nominals, targetBeg, targetEnd, 'nominal')

                label = [sourceBeg, targetBeg, polarity]
                annotations[begIdx].append(label)


        for wordIdx in range(len(annotations)):
            string = ''
            for labelIdx, label in enumerate(annotations[wordIdx]):
                srcRelPos = getRelPos(wordIdx, label[0], nominals)
                tgtRelPos = getRelPos(wordIdx, label[1], nominals)
                labelStr = ':'.join([polarity, srcRelPos, tgtRelPos])
                if labelIdx == 0:
                    string = labelStr
                else:
                    string += '|' + labelStr
            if string == '':
                string = '_'
            annotations[wordIdx] = string

        outFile.write('# sent_id=' + instance['sent_id'] + '\n')
        for wordIdx in range(len(words)):
            outFile.write('\t'.join([str(wordIdx+1), words[wordIdx], expressions[wordIdx], nominals[wordIdx], annotations[wordIdx]]) + '\n')
        outFile.write('\n')
        #print('# sent_id=' + instance['sent_id'])
        #for wordIdx in range(len(words)):
        #    print('\t'.join([str(wordIdx+1), words[wordIdx], expressions[wordIdx], nominals[wordIdx], annotations[wordIdx]]))
        #print()
    outFile.close()



os.system('mkdir -p test/task10')

for dataset in ['darmstadt_unis', 'mpqa', 'multibooked_ca', 'multibooked_eu', 'opener_en', 'opener_es', 'norec']:
    convert('data/task10/semeval22_structured_sentiment/data/' + dataset + '/test.json', 'test/task10/' + dataset + '.conll')
    for modelName in ['task10-optionB', 'multitask.rembert.emb.task10-optionB']:
        model = myutils.getModel(modelName)
        inPath = '../test/task10/' + dataset + '.conll'
        cmd = 'python3 predict.py ' + model[4:] + ' ' + inPath + ' ' + inPath + '.' + modelName
        print(cmd)

