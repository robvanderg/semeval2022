import myutils

for config in ['task10', 'task10-optionB']:
    langConfig, langEmbConfig, embConfig = myutils.genConfigs(config, ['darmstadt_unis', 'mpqa', 'multibooked_ca', 'multibooked_eu', 'norec', 'opener_en', 'opener_es'])

    for dataConfig in ['configs/' + config + '.json', langConfig, langEmbConfig]:
        myutils.train(dataConfig)


#for config in ['task10-cross', 'task10-cross-optionB']:
#    langConfig, langEmbConfig, embConfig = myutils.genConfigs(config, ['darmstadt_unis', 'mpqa', 'norec', 'opener_en'])
#
#    for dataConfig in ['configs/' + config + '.json', langConfig, langEmbConfig, embConfig]:
#        myutils.train(dataConfig)

