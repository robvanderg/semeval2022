import myutils
import ast
import os
import json
import scoreUtils

settings = ['task2-a1', 'task3-1', 'task3-2', 'task4-1', 'task4-2', 'task6-a', 'task6-b', 'task6-c', 'task10-optionB', 'task11', 'task12-optionB']
#settings = ['task11']

if not os.path.isdir('preds'):
    os.mkdir('preds')
print(' & '.join(['setup','base','-langs','langs-emb']))
allRows = []
for setting in settings:
    row = [setting,  scoreUtils.getScore(setting)]
    if myutils.getModel(setting + '-langs') != '':
        row.append(scoreUtils.getScore(setting + '-langs'))
    #if myutils.getModel(setting + '-langs-emb') != '':
    #    row.append(scoreUtils.getScore(setting + '-langs-emb'))
    #else:
    #    print('not found' + setting + '-langs-emb')
    row = [row[0]] + ['{:.2f}'.format(x) for x in row[1:]]
    if len(row) == 2:
        row.extend(['---'])#$, '---'])
    print(' & '.join(row) + ' \\\\')
    allRows.append(row)




