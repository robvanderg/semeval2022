import os
import myutils
import scoreUtils


configs1 = ['task2-a1-emb.json', 'task3-1-emb.json', 'task3-2-emb.json', 'task4-1-emb.json', 'task4-2-emb.json', 'task6-a-emb.json', 'task6-b-emb.json', 'task6-c-emb.json',  'task10-optionB-emb.json', 'task11-emb.json', 'task12-optionB-emb.json']
configs2 = [x.replace('-emb', '') for x in configs1]




for embed in ['mbert']:#['mbert', 'rembert']:
    allRows = []
    for config in configs2:
        row = [config.replace('.json', '')]
   
        if embed == 'mbert': 
            # single
            row.append(scoreUtils.getScore(config.replace('.json', '')))
        else:
            row.append(0.0)
        # multi
        row.append(scoreUtils.getScore('multitask.' + embed, config))
        # multi + fine
        row.append(scoreUtils.getScore('multitask.' + embed + '.' + config))
        row = [row[0]] + ['{:.2f}'.format(x) for x in row[1:]]
        print(' & '.join(row) + ' \\\\')
        allRows.append(row)
    avg1 = sum([float(x[1]) for x in allRows])/len(allRows)
    avg2 = sum([float(x[2]) for x in allRows])/len(allRows)
    avg3 = sum([float(x[3]) for x in allRows])/len(allRows)
    base = [float(x[1]) for x in allRows]
    multi = [float(x[2]) for x in allRows]
    multi_fine = [float(x[3]) for x in allRows]
    names = [x[0].replace('-optionB','') for x in allRows]
    myutils.graph([base, multi, multi_fine], names, ['Single', 'Multi', 'Multi_fine'], 'multi.pdf')
    #print(' & ' .join(['avg.', '{:.2f}'.format(avg1), '{:.2f}'.format(avg2), '{:.2f}'.format(avg3)]) + '\\\\')
