cd data
mkdir task12

# Download data from: https://competitions.codalab.org/competitions/34011#participate-get_starting_kit
cp ~/Downloads/public_data.zip .
unzip public_data.zip
rm public_data.zip 
cp ~/Downloads/symlink_dev_test.zip .
unzip symlink_dev_test.zip
rm symlink_dev_test.zip

cd ../

python3 scripts/0.prep.task12.py

