import myutils
import os
import json

os.system('mkdir -p test/task10')
os.system('mkdir -p test/task10/multi')
os.system('mkdir -p test/task10/multi/monolingual')
os.system('mkdir -p test/task10/single')
os.system('mkdir -p test/task10/single/monolingual')

for dataset in ['darmstadt_unis', 'mpqa', 'multibooked_ca', 'multibooked_eu', 'opener_en', 'opener_es', 'norec']:
#    for modelName in ['task10-optionB', 'multitask.rembert.emb.task10-optionB']:
    for modelName in ['multitask.rembert.emb.task10-optionB']:
        inPath = 'test/task10/' + dataset + '.conll.' + modelName
    
        cmd = 'python3 scripts/2.task10.convertBack.py ' + inPath
        print(cmd)
        os.system(cmd)
        if 'multi' in inPath:
            os.system('mkdir -p test/task10/multi/monolingual/' + dataset)
            os.system('mv ' + inPath + '.converted test/task10/multi/monolingual/' + dataset + '/predictions.json')
        else:
            os.system('mkdir -p test/task10/single/monolingual/' + dataset)
            os.system('mv ' + inPath + '.converted test/task10/single/monolingual/' + dataset + '/predictions.json')

