./scripts/0.prep.sh

# single task models
python3 scripts/1.train.task2.py > 1.train2.sh
./1.train2.sh
python3 scripts/1.train.task3.py > 1.train3.sh
./1.train3.sh
python3 scripts/1.train.task4.py > 1.train4.sh
./1.train4.sh
python3 scripts/1.train.task6.py > 1.train6.sh
./1.train6.sh
python3 scripts/1.train.task10.py > 1.train10.sh
./1.train10.sh
python3 scripts/1.train.task11.py > 1.train11.sh
./1.train11.sh
python3 scripts/1.train.task12.py > 1.train12.sh
./1.train12.sh

# multi-task model 
python3 scripts/3.train-multitask-pretrain.py > 3.train.sh
./3.train.sh

# multi-task model + finetune
python3 scripts/4.train-multitask-finetune.py > 4.train.sh
./4.train.sh

# There is also code for running on the test data, its in scripts/5* 
# This code is clead a bit less, as I only needed it once
