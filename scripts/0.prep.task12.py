import json
import pprint
import os
import unicodedata

def _is_punctuation(char):
    """Checks whether `char` is a punctuation character."""
    cp = ord(char)
    # We treat all non-letter/number ASCII as punctuation.
    # Characters such as "^", "$", and "`" are not in the Unicode
    # Punctuation class but we treat them as punctuation anyways, for
    # consistency.
    if (cp >= 33 and cp <= 47) or (cp >= 58 and cp <= 64) or (cp >= 91 and cp <= 96) or (cp >= 123 and cp <= 126):
        return True
    cat = unicodedata.category(char)
    if cat.startswith("P"):
        return True
    return False


def convertRel(relation, relIdx, data):
    target = relation.split(':')[0]
    targetIdx = -1
    tOther = int(relation.split(':')[0][1:])
    for rowIdx, row in enumerate(data):
        for ne in row[1]:
            if '-' + target + ':' in ne:
                targetIdx = rowIdx
    if relIdx < targetIdx:
        idxsRange = range(relIdx+1, len(data))
        direction = '+'
    else:
        idxsRange = reversed(range(0, relIdx-1))
        direction = '-'

    counter = 0
    broken = False
    for wordIdx in idxsRange:
        if len(data[wordIdx][1]) > 0 and not broken:
            for annotation in data[wordIdx][1]:
                if annotation[0] == 'B':
                    counter += 1
                    if 'T' + str(tOther) + ':' in annotation:# TODO, this last 0 should be a loop
                        broken = True
                        break
    return direction + str(counter) + ':' + relation.split(':')[1]

    
# the general setup is like: 
# switch to character level annotation
# tokenize text
# loop through orig text, tokked text and annotation, and insert spaces in annotation (when space found in tokked but not in orig.
# convert annotation to relative annotation

def convert(path, outFile):
    print(path)
    data = json.load(open(path))
    for instanceIdx, instance in enumerate(data):
        #if instance != '1511.07373v1.What_is_the_plausibility_of_probability_revised_2003_2015/paragraph_59':
        #    continue
        text = data[instance]['text'].replace('\t', ' ').replace('\n', ' ')
        oldText = text

        # get character level annotation
        entities = [[] for _ in range(len(text))]
        for entity in data[instance]['entity']:
            for i in range(data[instance]['entity'][entity]['start'], data[instance]['entity'][entity]['end']):
                entities[i].append(entity + ':' + data[instance]['entity'][entity]['label'])
        
        # find new whitespaces to add
        addSpaceIdxs = []
        for charIdx, char in enumerate(text):
            if _is_punctuation(char):
                if charIdx != 0 and text[charIdx-1] != ' ':
                    addSpaceIdxs.append(charIdx)
                if charIdx < len(text)-1 and text[charIdx+1] != ' ':
                    addSpaceIdxs.append(charIdx+1)
            # separate numbers
            if char in '0123456789':
                if charIdx != 0 and text[charIdx-1] not in [' 0123456789,.']:
                    addSpaceIdxs.append(charIdx)
                if charIdx < len(text)-1 and text[charIdx+1] not in [' 0123456789,.']:
                    addSpaceIdxs.append(charIdx+1)

        # insert the whitespaces and move the annotation
        # do this reversed, so that indexes don't change when inserting spaces
        text = [c for c in text]
        for charIdx in reversed(range(len(text))):
            if charIdx in addSpaceIdxs:
                text.insert(charIdx, ' ')
                entities.insert(charIdx, []) #could add label as well here if necessary?

        # convert to the newData format which a list-item per word.
        newData = []
        curWord = []
        curAnnotation = []
        curMeta = 1 # the number of whitespaces
        for charIdx in range(len(text)):
            if text[charIdx] in [' ', '\t', '\n']:
                if curWord != []:
                    if newData == []:
                        curMeta -= 1
                    newData.append([''.join(curWord), curAnnotation, curMeta])
                    curWord = []
                    curAnnotation = []
                    curMeta = 1
                else:
                    curMeta += 1
            else:
                curWord.append(text[charIdx])
                curAnnotation.append(entities[charIdx])
        
        # Sometimes annotation spans do not mach words exactly, sometimes this
        # seems to be an annotation mistake. i.e. 'sets' -> only 'set' has the label
        # in other samples, things like a formula part `kY' needs the `y´  
        # annotated separately, we cant handle this, and give the whole token
        # the same label here
        seen = set()
        for word in newData:
            allAnnotations = [item for sublist in word[1] for item in sublist]
            word[1] = list(set(allAnnotations))
            for annotationIdx in range(len(word[1])):
                if 'B-' + word[1][annotationIdx] in seen:
                    word[1][annotationIdx] = 'I-' + word[1][annotationIdx]
                else:
                    word[1][annotationIdx] = 'B-' + word[1][annotationIdx]
                    seen.add(word[1][annotationIdx])
                    
            

        # convert relations to dict for easy lookup
        # note: arg 0 is not unique 3002 times, arg1 3810 times
        relations = {}
        for relation in data[instance]['relation']:
            arg0 = data[instance]['relation'][relation]['arg0']
            arg1 = data[instance]['relation'][relation]['arg1']
            label = data[instance]['relation'][relation]['label']
            if arg0 not in relations:
                relations[arg0] = []
            relations[arg0].append(arg1 + ':' + label)

            

        # fix the number of spaces, because we added some of them manually
        charIdx = 0
        for wordIdx, word in enumerate(newData):
            if charIdx in addSpaceIdxs:
                word[-1] -= 1
            charIdx += len(word[0]) + word[2]

        # printing
        for meta in ['id', 'phase', 'topic',  'document', 'paragraph', 'prefix']:
            outFile.write('# ' + meta + ': ' + data[instance][meta] + '\n')
        #outFile.write('# addedspaces: ' + ','.join([str(x) for x in addSpaceIdxs]) + '\n')
        prevlabels = {}
        charIdx = 0
        for wordIdx, word in enumerate(newData):
            relationStrings = []
            for annotation in word[1]:
                if annotation[0] == 'B' and annotation[2:].split(':')[0] in relations:
                    for relation in relations[annotation[2:].split(':')[0]]:
                        relationConverted = convertRel(relation, wordIdx, newData)
                        relationStrings.append(relationConverted)# + ' - ' + relation ) 
            prevlabels = word[1]
            if relationStrings == []:
                relationStrings = ['_']
            entities = [annotation.split('T')[0] + annotation.split(':')[1] for annotation in word[1]]
            if entities == []:
                entities = ['O']
            outFile.write('\t'.join([str(wordIdx+1), word[0], '|'.join(entities), '|'.join(relationStrings), 'whitespaceBefore=' + str(word[2])]) + '\n')
        outFile.write('\n')
        
        
trainFile = open('data/task12/train.all.conll', 'w')
devFile = open('data/task12/dev.all.conll', 'w')

for jsonFile in os.listdir('data/task12/public_data/'):
    if jsonFile.endswith('.json'):
        convert('data/task12/public_data/' + jsonFile, trainFile)
for jsonFile in os.listdir('data/task12/symlink_dev_test/data_dev_reference/'):
    if jsonFile.endswith('.json'):
        convert('data/task12/symlink_dev_test/data_dev_reference/' + jsonFile, devFile)

trainFile.close()
devFile.close()

