cd data
mkdir task11
cd task11
cp ~/Downloads/training_data.zip .
unzip training_data.zip
cd ../../
python3 scripts/0.prep.task11.py


# There are many newlines in this file that break my scripts
sed -i -e '/./b' -e :n -e 'N;s/\n$//;tn' data/task11/dev.mi.conll
sed -i -e "1d" data/task11/dev.mi.conll
