import csv
import random

random.seed(8446)

def csv2data(path, delimiter=',' ) :
    header = None
    data   = list()
    with open(path, encoding='utf-8') as csvfile:
      reader = csv.reader( csvfile, delimiter=delimiter )
      for row in reader :
          if header is None :
              header = row
              continue
          data.append( row )
    return data

def csv2conll(path):
    data = csv2data(path)
    outPath = open(path + '.conll', 'w')
    for item in data:
        item = [x.replace('\t', ' ').replace('\n', ' ') for x in item]
        del item[3]
        outPath.write('\t'.join(item) + '\n')
    outPath.close()

arData = csv2data('data/task6/train.Ar.csv')
enData = csv2data('data/task6/train.En.csv')

arSplit = int(.8 *len(arData))
enSplit = int(.8 * len(enData))

def write(data, path):
    outFile = open(path, 'w')
    for item in data:
        item = [x.replace('\t', ' ').replace('\n', ' ') for x in item]
        outFile.write('\t'.join(item) + '\n')
    outFile.close()

enDataB = enData[:866]
random.shuffle(enDataB)
# In En, only the first 866 have fine-grained annotation
write(enDataB[:692], 'data/task6/2.train.en.conll')
write(enDataB[692:866], 'data/task6/2.dev.en.conll')



arDataC = arData[:746]
random.shuffle(arDataC)
for itemIdx in range(len(arDataC)):
    if random.random() > .5:
        tmp = arDataC[itemIdx][1]
        arDataC[itemIdx][1] = arDataC[itemIdx][3]
        arDataC[itemIdx][3] = tmp
        arDataC[itemIdx][2] = '0'
for itemIdx in range(len(enDataB)):
    if random.random() > .5:
        tmp = enDataB[itemIdx][1]
        enDataB[itemIdx][1] = enDataB[itemIdx][3]
        enDataB[itemIdx][3] = tmp
        enDataB[itemIdx][2] = '0'

write(enDataB[:692], 'data/task6/3.train.en.conll')
write(arDataC[:596], 'data/task6/3.train.ar.conll')
write(enDataB[:692] + arDataC[:596], 'data/task6/3.train.all.conll')
write(enDataB[692:866] + arDataC[596:746], 'data/task6/3.dev.all.conll')
write(enDataB[692:866], 'data/task6/3.dev.en.conll')
write(arDataC[596:746], 'data/task6/3.dev.ar.conll')

    
random.shuffle(enData)
random.shuffle(arData)

write(enData[:enSplit], 'data/task6/1.train.en.conll')
write(arData[:arSplit], 'data/task6/1.train.ar.conll')
write(enData[enSplit:], 'data/task6/1.dev.en.conll')
write(arData[arSplit:], 'data/task6/1.dev.ar.conll')
write(enData[:enSplit] + arData[:arSplit], 'data/task6/1.train.all.conll')
write(enData[enSplit:] + arData[arSplit:], 'data/task6/1.dev.all.conll')


