import myutils
import os
import json

os.system('mkdir -p test/task12/multi')
os.system('mkdir -p test/task12/single')

datasets = []
for jsonfile in os.listdir('data/task12/symlink_dev_test/data_test/'):
    if jsonfile.endswith('json'):        
        datasets.append(jsonfile)

for modelName in ['multitask.rembert.emb.task12-optionB', 'task12-optionB']:
    for dataset in datasets:
        predPath = 'test/task12/' + dataset + '-' + modelName
        outPath = 'test/task12/single/' + dataset
        if 'multi' in modelName:
            outPath = 'test/task12/multi/' + dataset
        cmd = 'python3 scripts/2.task12.convertBack.py ' + predPath 
        os.system(cmd)
        os.system('mv ' + predPath + '.converted ' + outPath)

