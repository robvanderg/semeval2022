import os
import myutils
# TODO filter one setup for each task!
configs = []
#for config in os.listdir('configs/'):
#    if config.startswith('task'):
#        configs.append('../configs/' + config)


configs1 = ['task2-a1-emb.json', 'task3-1-emb.json', 'task3-2-emb.json', 'task4-1-emb.json', 'task4-2-emb.json', 'task6-a-emb.json', 'task6-b-emb.json', 'task6-c-emb.json',  'task10-optionB-emb.json', 'task11-emb.json', 'task12-optionB-emb.json']
configs1 = ['../configs/' + x for x in configs1]
configs2 = [x.replace('-emb', '') for x in configs1]
for configs, name in zip([configs1, configs2], ['.emb', '']):
# pre-train
    for embed, params in zip(['mbert', 'xlmr', 'rembert', 'twitter', 'deberta', 'mluke'], ['params.smoothSampling', 'params-xlmr-smooth', 'params-rembert-smooth', 'params-twitter-smooth', 'params-deberta-smooth', 'params-mluke-smooth']):
        modelName = 'multitask.' + embed + name
        if myutils.getModel(modelName) == '':
            cmd = 'python3 train.py --dataset_configs ' + ' '.join(configs) + ' --parameters_config ../configs/' + params + '.json --name ' + modelName
            print(cmd)


configs1 = ['task2-a1-emb.json', 'task3-1-emb.json', 'task3-2-emb.json', 'task4-1-emb.json', 'task4-2-emb.json', 'task6-a-emb.json', 'task6-b-emb.json', 'task6-c-emb.json',  'task10-optionB-emb.json', 'task11-emb.json']#, 'task12-optionB-emb.json']
configs1 = ['../configs/' + x for x in configs1]
configs2 = [x.replace('-emb', '') for x in configs1]
for configs, name in zip([configs1, configs2], ['.emb', '']):
# pre-train
    for embed, params in zip(['mbert', 'xlmr', 'rembert', 'twitter', 'deberta', 'mluke'], ['params.smoothSampling', 'params-xlmr-smooth', 'params-rembert-smooth', 'params-twitter-smooth', 'params-deberta-smooth', 'params-mluke-smooth']):
        modelName = 'multitask.' + embed + name + '-smaller'
        if myutils.getModel(modelName) == '':
            cmd = 'python3 train.py --dataset_configs ' + ' '.join(configs) + ' --parameters_config ../configs/' + params + '.json --name ' + modelName
            print(cmd)
    


