import os

os.system('mkdir -p test/task11/single/')
os.system('mkdir -p test/task11/multi/')

def getTest(path):
    for testFile in os.listdir(path):
        if 'test' in testFile:
            return path + '/' + testFile

for dataDir in os.listdir('data/task11/test/'):
    if os.path.isdir('data/task11/test/' + dataDir):
        testFile = getTest('data/task11/test/' + dataDir)
        lang = testFile.split('/')[-1].split('_')[0]

        for modelName in ['task11', 'multitask.rembert.emb.task11']:
            predPath = 'test/task11/' + lang + '.' + modelName
            outPath = 'test/task11/single/'
            if 'multi' in modelName:
                outPath = 'test/task11/multi/'
            outPath += lang + '.pred.conll'
            cmd = 'grep -v "^# id" ' + predPath + ' | cut -f 4 > ' + outPath
            print(cmd)
            os.system(cmd)
            

