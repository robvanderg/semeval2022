import json
import math
configs1 = ['task2-a1-emb.json', 'task3-1-emb.json', 'task3-2-emb.json', 'task4-1-emb.json', 'task4-2-emb.json', 'task6-a-emb.json', 'task6-b-emb.json', 'task6-c-emb.json',  'task10-optionB-emb.json', 'task11-emb.json', 'task12-optionB-emb.json']
configs2 = [x.replace('-emb', '').replace('.json', '') for x in configs1]

def getCountsSents(path, sent_idxs):
    wordCount = 0
    for sentCount, line in enumerate(open(path)):
        tok = line.strip().split('\t')
        for sentIdx in sent_idxs:
            wordCount += tok[sentIdx].count(' ')
    return wordCount, sentCount

def getCountsWords(path, word_idx):
    wordCount = 0
    sentCount = 0
    for line in open(path):
        if len(line) > 2:
            if line[0] == '#' and '\t' not in line:
                continue
            else:
                wordCount += 1
        else:
            sentCount += 1
    return wordCount, sentCount

table = []
for task in configs2:
    row = [task.replace('-optionB', '')]
    data = json.load(open('configs/' + task + '.json'))
    name = [x for x in data][0]
    tasks = [data[name]['tasks'][x]['task_type'] for x in data[name]['tasks']]
    if tasks.count('classification') > 2:
        tasks = ['classification*' + str(tasks.count('classification'))]
    row.append(' '.join(tasks))
    if 'sent_idxs' in data[name]:
        numWords, numSents = getCountsSents(data[name]['train_data_path'][3:], data[name]['sent_idxs'])
    else:
        numWords, numSents = getCountsWords(data[name]['train_data_path'][3:], data[name]['word_idx'])
    row.extend([f"{numWords:,}", f"{numSents:,}"])
    table.append(row)

def getSmoothedSizes(smooth_factor, sizes):
    # calculate new size based on smoothing
    new_sizes = []
    total_size = sum(sizes)
    total_new_prob = 0.0
    for size in sizes:
        pi = size/total_size
        total_new_prob += math.pow(pi, smooth_factor)

    for size in sizes:
        pi = size/total_size
        prob = (1/pi) * (math.pow(pi, smooth_factor)/total_new_prob)
        new_sizes.append(int(size * prob))
    return new_sizes

sizes = [int(row[-1].replace(',', '')) for row in table]
print(sizes)
alpha = .5
newSizes = getSmoothedSizes(alpha, sizes)
for i in range(len(newSizes)):
    table[i].append(f"{newSizes[i]:,}")


for row in table:
    print(' & '.join([str(x) for x in row]) + '\\\\')

