import os
import myutils

os.system('mkdir -p test/task3')
finalFile = 'test/task3/answer.tsv'

models = ['task3-1', 'multitask.rembert.task3-1']
task1folder = 'data/task3/SemEval2022Task3/data/test/official_test_sets/subtask-1/'
for testFile in os.listdir(task1folder):
    for modelName in models:
        model = myutils.getModel(modelName)
        lang = testFile.split('.')[0]
        outFile = '../preds/test-' + modelName + '.' + lang
        cmd = 'python3 predict.py ' + model[4:] + ' ../' + task1folder + testFile + ' ' + outFile
        print(cmd)
        #os.system('sed \'1d\' ' + outFile + ' >> ' + finalFile
    
models = ['task3-2', 'multitask.rembert.task3-2']
task2folder = 'data/task3/SemEval2022Task3/data/test/official_test_sets/subtask-2/'
for testFile in os.listdir(task2folder):
    for modelName in models:
        model = myutils.getModel(modelName)
        lang = testFile.split('.')[0]
        outFile = '../preds/test-' + modelName + '.' + lang
        cmd = 'python3 predict.py ' + model[4:] + ' ../' + task2folder + testFile + ' ' + outFile
        print(cmd)
        #os.system('sed \'1d\' ' + outFile + ' >> ' + finalFile

