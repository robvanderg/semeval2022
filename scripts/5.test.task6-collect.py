import myutils
import os
import csv
import myutils

os.system('mkdir -p test/task6/')
os.system('mkdir -p test/task6/single')
os.system('mkdir -p test/task6/multi')

for modelName in ['task6-a', 'multitask.rembert.emb.task6-a']:
    for tgtFile in ['taskA.En.input.csv', 'task_A_AR_test.csv']:
        inPath = 'data/task6/test/' + tgtFile + '.cleaned.' + modelName
        outPath = 'test/task6/single/' + tgtFile.replace('csv', 'txt')
        if 'multi' in modelName:
            outPath = 'test/task6/multi/' + tgtFile.replace('csv', 'txt')
        if 'En' in tgtFile:
            os.system('echo "task_a_en" > ' + outPath)
        else:
            os.system('echo "task_a_ar" > ' + outPath)
        os.system('cut -f 3 ' + inPath + ' >> ' + outPath)

for modelName in ['task6-b', 'multitask.mbert.emb.task6-b']:
    for tgtFile in ['taskB.En.input.csv']:
        inPath = 'data/task6/test/' + tgtFile + '.cleaned.' + modelName
        outPath = 'test/task6/single/' + tgtFile.replace('csv', 'txt')
        if 'multi' in modelName:
            outPath = 'test/task6/multi/' + tgtFile.replace('csv', 'txt')
        os.system('echo "sarcasm,irony,satire,understatement,overstatement,rhetorical_question" > ' + outPath)
        os.system('cut -f 5-10 ' + inPath + ' | sed "s;\t;,;g" >> ' + outPath)

for modelName in ['task6-c', 'multitask.mbert.task6-c']:
    for tgtFile in ['taskC.En.input.csv', 'task_C_AR_test.csv']:
        inPath = 'data/task6/test/' + tgtFile + '.cleaned.' + modelName
        outPath = 'test/task6/single/' + tgtFile.replace('csv', 'txt')
        if 'multi' in modelName:
            outPath = 'test/task6/multi/' + tgtFile.replace('csv', 'txt')
        if 'En' in tgtFile:
            os.system('echo "task_c_en" > ' + outPath)
        else:
            os.system('echo "task_c_ar" > ' + outPath)
        os.system('cut -f 3 ' + inPath + ' >> ' + outPath)


