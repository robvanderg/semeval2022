import myutils

for config, langs  in zip(['task6-a', 'task6-b', 'task6-c'], [['en', 'ar'], ['en'], ['en', 'ar']]):

    if len(langs) > 1:
        langConfig, langEmbConfig, embConfig = myutils.genConfigs(config, langs)
        for dataConfig in ['configs/' + config + '.json', langConfig, langEmbConfig]:
            myutils.train(dataConfig)
    else:
        myutils.train('configs/' + config + '.json')

