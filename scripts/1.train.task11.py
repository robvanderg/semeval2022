import myutils

config = 'task11'
langConfig, langEmbConfig, embConfig = myutils.genConfigs(config, ['bn', 'de', 'en', 'es', 'fa', 'hi', 'ko', 'mi', 'nl', 'ru', 'tr', 'zh'])

for dataConfig in ['configs/' + config + '.json', langConfig, langEmbConfig]:
    myutils.train(dataConfig)

config = 'task11-optionB'
langConfig, langEmbConfig, embConfig = myutils.genConfigs(config, ['bn', 'de', 'en', 'es', 'fa', 'hi', 'ko', 'mi', 'nl', 'ru', 'tr', 'zh'])

for dataConfig in ['configs/' + config + '.json', langConfig, langEmbConfig]:
    myutils.train(dataConfig)

