import os
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt


def genConfigs(configName, langs):
    from allennlp.common import Params
    # generate language separate training
    newParams = {}
    for lang in langs:
        params = Params.from_file('configs/' + configName + '.json')
        name = list(params.as_dict())[0] + '.' + lang
        newParams[name] = params[list(params.as_dict())[0]].as_dict()
        newParams[name]['train_data_path'] = newParams[name]['train_data_path'].replace('all', lang)
        if 'cross' not in configName:
            newParams[name]['validation_data_path'] = newParams[name]['validation_data_path'].replace('all', lang)

        tasks = list(newParams[name]['tasks'])
        for task in tasks:
            newParams[name]['tasks'][task + '-' + lang] = newParams[name]['tasks'].pop(task)

    Params(newParams).to_file('configs/' + configName + '-langs.json')

    for dataset in newParams:
        newParams[dataset]['enc_dataset_embed_idx'] = -1
    Params(newParams).to_file('configs/' + configName + '-langs-emb.json')

    for dataset in newParams:
        tasks = list(newParams[dataset]['tasks'])
        for task in tasks:
            newParams[dataset]['tasks'][task[:task.rfind('-')]] = newParams[dataset]['tasks'].pop(task)
    Params(newParams).to_file('configs/' + configName + '-emb.json')
            

    return 'configs/' + configName + '-langs.json', 'configs/' + configName + '-langs-emb.json', 'configs/' + configName + '-emb.json'

def getModel(name):
    name = name.replace('.json', '').replace('configs/', '')
    modelDir = 'mtp/logs/'
    nameDir = modelDir + name + '/'
    if os.path.isdir(nameDir):
        for modelDir in reversed(sorted(os.listdir(nameDir))):
            modelPath = nameDir + modelDir + '/model.tar.gz'
            if os.path.isfile(modelPath):
                return modelPath
    return ''


def train(dataConfig, paramsConfig=None, name=None):
    cmd = 'python3 train.py --dataset_config ../' + dataConfig
    if paramsConfig != None:
        cmd += ' --parameters_config ../' + paramsConfig
    if name != None:
        cmd += ' --name ' + name
    else:
        name = dataConfig
    if getModel(name) == '':
        print(cmd)

def graph(scores, names, names2, outName):
    assert len(scores[0]) == len(names)
    assert len(scores) == len(names2)
    plt.style.use('scripts/niceGraphs.mplstyle')
    fig, ax = plt.subplots(figsize=(15,4), dpi=300)
    colors = plt.rcParams["axes.prop_cycle"].by_key()["color"]
    colors = colors + colors
    
    width = 1/(len(scores)+1)
    for modelIdx in range(len(scores)):
        dist = modelIdx-int(len(scores)/2)
        if len(scores)%2 == 0:
            x = [idx+dist*width+.5*width for idx in range(len(scores[0]))]
        else:
            x = [idx+dist*width for idx in range(len(scores[0]))]
        print(x)
        ax.bar(x, scores[modelIdx], color=colors[modelIdx], width=width, label=names2[modelIdx])

    leg = ax.legend()
    leg.get_frame().set_linewidth(1.5)
    ax.set_xlim(-.5, len(names)-.5)
    ax.set_xticks(range(len(names)))
    ax.set_xticklabels(names, rotation=25, ha='right')
    fig.savefig(outName, bbox_inches='tight')




