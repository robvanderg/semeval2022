import os

cats = ['Unbalanced_power_relations', 'Shallow_solution', 'Presupposition', 'Authority_voice', 'Metaphors', 'Compassion', 'The_poorer_the_merrier']
def convert42(inFile, outFile):
    f = open(outFile, 'w')
    for line in open(inFile):
        label = line.strip().split('\t')[8]
        outList = ['0'] * len(cats)
        labelIdx = cats.index(label)
        outList[labelIdx] = '1'
        f.write(','.join(outList) + '\n')
    f.close()


os.system('mkdir -p test/task4')

os.system('mkdir -p test/task4/single')
os.system('cut -f 6 data/task4/dontpatronizeme/semeval-2022/TEST/task4_test.conll-task4-1 > test/task4/single/task1.txt')
convert42('data/task4/dontpatronizeme/semeval-2022/TEST/task4_test.conll-task4-2', 'test/task4/single/task2.txt')

os.system('mkdir -p test/task4/multi')
os.system('cut -f 6 data/task4/dontpatronizeme/semeval-2022/TEST/task4_test.conll-multitask.mbert.task4-1 > test/task4/multi/task1.txt')
convert42('data/task4/dontpatronizeme/semeval-2022/TEST/task4_test.conll-multitask.rembert.emb.task4-2', 'test/task4/multi/task2.txt')


