import myutils

config = 'task2-a1'
langConfig, langEmbConfig, embConfig = myutils.genConfigs(config, ['en', 'pt'])


for dataConfig in ['configs/' + config + '.json', langConfig, langEmbConfig]:
    myutils.train(dataConfig)

