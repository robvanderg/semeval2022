import os

dataDir = 'data/task11/'

def clean(path):
    outFile = open(path + '.cleaned', 'w')
    for line in open(path):
        if len(line) < 2:
            outFile.write('\n')
        elif line.startswith('# id '):
            outFile.write(line.replace('\t', ' '))
        else:
            outFile.write(line.replace(' ', '\t'))
    outFile.close()


for lang in os.listdir(dataDir):
    if lang[0].upper() !=lang[0]:
        continue
    for dataFile in os.listdir(dataDir + lang):
        if dataFile.endswith('conll'):
            clean(dataDir + lang + '/' + dataFile)

langs = ['BN-Bangla', 'DE-German', 'EN-English', 'ES-Spanish', 'FA-Farsi', 'HI-Hindi', 'KO-Korean', 'MIX_Code_mixed', 'NL-Dutch', 'RU-Russian', 'TR-Turkish', 'ZH-Chinese']
for lang in langs:
    cmd = 'cp ' + dataDir + lang + '/*train*cleaned data/task11/train.'  + lang[:2].lower() + '.conll'
    os.system(cmd)
    cmd = 'cp ' + dataDir + lang + '/*dev*cleaned data/task11/dev.'  + lang[:2].lower() + '.conll'
    os.system(cmd)

cmd = 'cat ' + ' '.join(dataDir + lang + '/*train*cleaned' for lang in langs) + ' > ' + dataDir + 'train.all.conll'
os.system(cmd)
cmd = 'cat ' + ' '.join(dataDir + lang + '/*dev*cleaned' for lang in langs) + ' > ' + dataDir + 'dev.all.conll'
os.system(cmd)

