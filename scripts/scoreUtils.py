import myutils
import ast
import os
import json

settings = ['task2-a1', 'task3-1', 'task3-2', 'task4-1', 'task4-2', 'task6-a', 'task6-b', 'task6-c', 'task10', 'task10-optionB', 'task11', 'task12', 'task12-optionB']


monolingual_datasets = ["norec", "multibooked_ca", "multibooked_eu", "opener_en", "opener_es", "mpqa", "darmstadt_unis"]
crosslingual_datasets = ["opener_es", "multibooked_ca", "multibooked_eu"]

def json2lookup(path):
    lookup = {}
    data = json.load(open(path))
    for item in data:
        lookup[item['sent_id']] = item
    return lookup

def getScore10(model):
    name = model.split('/')[-3]
    os.system('mkdir -p preds/' + name)
    os.system('mkdir -p preds/' + name + '/res')
    os.system('mkdir -p preds/' + name + '/res/monolingual')
    os.system('mkdir -p preds/' + name + '/res/crosslingual')
    os.system('mkdir -p preds/' + name + '/ref/data')
    os.system('mkdir -p preds/' + name + '/ref/data/monolingual')
    os.system('mkdir -p preds/' + name + '/ref/data/crosslingual')
    
    # monolingual first:
    if ('emb' not in model and 'langs' not in model) or 'multitask' in model and '.task' in model and 'multitask.rembert.emb/' not in model:
        # This is a bit cumbersome, as we have to split the data again
        cmd = 'python3 scripts/2.task10.convertBack.py ' + model.replace('model.tar.gz', 'SEMEVAL10.dev.out')
        os.system(cmd)
        lookup = json2lookup(model.replace('model.tar.gz', 'SEMEVAL10.dev.out') + '.converted')
        for dataset in monolingual_datasets:
            preds = []
            for tgtInstance in json.load(open('data/task10/semeval22_structured_sentiment/data/'+ dataset + '/dev.json')):
                tgtId = tgtInstance['sent_id']
                preds.append(lookup[tgtId])
            os.system('mkdir -p preds/' + name + '/res/monolingual/' + dataset)
            predPath = 'preds/' + name + '/res/monolingual/' + dataset + '/predictions.json'
            json.dump(preds, open(predPath, 'w'))

            goldPath = 'data/task10/semeval22_structured_sentiment/data/'+ dataset + '/dev.json'
            os.system('mkdir -p preds/' + name + '/ref/data/monolingual/' + dataset)
            os.system('cp ' + goldPath + ' preds/' + name + '/ref/data/monolingual/' + dataset + '/test.json')
    else:
        for dataset in monolingual_datasets:
            predPath = model.replace('model.tar.gz', 'SEMEVAL10.' + dataset + '.dev.out')
            cmd = 'python3 scripts/2.task10.convertBack.py ' + predPath
            os.system(cmd)
            os.system('mkdir -p preds/' + name + '/res/monolingual/' + dataset)
            os.system('cp ' + predPath + '.converted preds/' + name + '/res/monolingual/' + dataset + '/predictions.json')

            goldPath = 'data/task10/semeval22_structured_sentiment/data/'+ dataset + '/dev.json'
            os.system('mkdir -p preds/' + name + '/ref/data/monolingual/' + dataset)
            os.system('cp ' + goldPath + ' preds/' + name + '/ref/data/monolingual/' + dataset + '/test.json')
    
    # crosslingual
    #model = myutils.getModel(name[:6] + '-cross' + name[6:])
    #if 'emb' not in model[-15:] and 'langs' not in model:
    #    # This is a bit cumbersome, as we have to split the data again
    #    cmd = 'python3 scripts/2.task10.convertBack.py ' + model.replace('model.tar.gz', 'SEMEVAL10.dev.out')
    #    os.system(cmd)
    #    lookup = json2lookup(model.replace('model.tar.gz', 'SEMEVAL10.dev.out') + '.converted')
    #    for dataset in crosslingual_datasets:
    #        preds = []
    #        for tgtInstance in json.load(open('data/task10/semeval22_structured_sentiment/data/'+ dataset + '/dev.json')):
    #            tgtId = tgtInstance['sent_id']
    #            preds.append(lookup[tgtId])
    #        os.system('mkdir -p preds/' + name + '/res/crosslingual/' + dataset)
    #        predPath = 'preds/' + name + '/res/crosslingual/' + dataset + '/predictions.json'
    #        json.dump(preds, open(predPath, 'w'))

    #        goldPath = 'data/task10/semeval22_structured_sentiment/data/'+ dataset + '/dev.json'
    #        os.system('mkdir -p preds/' + name + '/ref/data/crosslingual/' + dataset)
    #        os.system('cp ' + goldPath + ' preds/' + name + '/ref/data/crosslingual/' + dataset + '/test.json')
    #else:
    #    for dataset in crosslingual_datasets:
    #        predPath = model.replace('model.tar.gz', 'SEMEVAL10.' + dataset + '.dev.out')
    #        cmd = 'python3 scripts/2.task10.convertBack.py ' + predPath
    #        os.system(cmd)
    #        os.system('cp ' + predPath + '.converted preds/' + name + '/res/crosslingual/' + dataset + '/predictions.json')

    #        goldPath = 'data/task10/semeval22_structured_sentiment/data/'+ dataset + '/dev.json'
    #        os.system('mkdir -p preds/' + name + '/ref/data/crosslingual/' + dataset)
    #        os.system('cp ' + goldPath + ' preds/' + name + '/ref/data/crosslingual/' + dataset + '/test.json')
    cmd = 'python3 data/task10/semeval22_structured_sentiment/evaluation/evaluate.py preds/' + name + '/ preds/' + name + '/ > tmp'
    os.system(cmd)
    scores = []
    for line in open('tmp'):
        if line.startswith('Average'):
            scores.append(float(line.strip().split(' ')[-1])*100)
    return scores[0]

def readConll(path):
    data = []
    curSent = []
    for line in open(path):
        if len(line) < 2:
            data.append(curSent)
            curSent = []
        else:
            curSent.append(line.strip())
    return data

def convert11(predIn, begIdx, endIdx, goldIn, outPath):
    sentCounter = 0
    predData = readConll(predIn)[begIdx:endIdx]
    goldData = readConll(goldIn)
    out = open(outPath, 'w')
    for predLine, goldLine in zip(predData, goldData):
        for predToken, goldToken in zip(predLine, goldLine):
            if goldToken.split('\t')[0].startswith('# '):
                continue
            tok = [goldToken.split('\t')[0], goldToken.split('\t')[-1], predToken.split('\t')[-1]]
            out.write(' '.join(tok) + '\n')
        out.write('\n')
    out.close()

def getScores11(model):

    name = model.split('/')[-3]
    os.system('mkdir -p preds/' + name)
    if ('emb' not in model and 'langs' not in model) or 'multitask' in model and '.task' in model:
        scores = []
        # SPLIT
        predPath = model.replace('model.tar.gz', 'SEMEVAL11.dev.out')
        begIdx = 0
        for lang in ['bn', 'de', 'en', 'es', 'fa', 'hi', 'ko', 'mi', 'nl', 'ru', 'tr', 'zh']:
            endIdx = begIdx + 800
            if lang == 'mi':
                endIdx -= 201
            goldPath = 'data/task11/dev.' + lang + '.conll'
            outPath = 'preds/' + name + '/dev.' + lang + '.conll'
            convert11(predPath, begIdx, endIdx, goldPath, outPath)
            cmd = 'perl data/task11/conlleval.pl < ' + outPath + ' > ' + outPath + '.eval'
            os.system(cmd)
            scores.append(float(open(outPath + '.eval').readlines()[1].strip().split(' ')[-1]))

            begIdx += 800
            if lang == 'mi':
                begIdx -= 300
        return sum(scores)/len(scores) 
    
    else:
        scores = []
        for lang in ['bn', 'de', 'en', 'es', 'fa', 'hi', 'ko', 'mi', 'nl', 'ru', 'tr', 'zh']:
            predPath = model.replace('model.tar.gz', 'SEMEVAL11.' + lang + '.dev.out')
            goldPath = 'data/task11/dev.' + lang + '.conll'
            outPath = 'preds/' + name + '/dev.' + lang + '.conll'
            cmd = 'paste ' + goldPath + ' ' + predPath + ' | cut -f 1,4,8 | grep -v "^#" | sed "s;	; ;g" > ' + outPath
            os.system(cmd)
            cmd = 'perl data/task11/conlleval.pl < ' + outPath + ' > ' + outPath + '.eval'
            os.system(cmd)
            scoreData = open(outPath + '.eval').readlines()
            if len(scoreData) == 0:
                print('error ' + goldPath + ' ' + predPath)
                scores.append(0.0)
            else:
                scores.append(float(open(outPath + '.eval').readlines()[1].strip().split(' ')[-1]))
        return sum(scores)/len(scores)
            

def getScore10old(model):
    scores = []
    modelDir = model[:model.rfind('/')]
    for outFile in os.listdir(modelDir):
        if outFile.endswith('out'):
            if outFile.split('.')[1] == 'dev':
                return 0.0
            cmd = 'python3 scripts/10.convertBack.py ' + modelDir + '/' + outFile 
            os.system(cmd)
            gold = 'data/task10/semeval22_structured_sentiment/data/' + outFile.split('.')[1] + '/dev.json'
            cmd = 'python3 data/task10/semeval22_structured_sentiment/evaluation/evaluate_single_dataset.py ' + gold + ' ' + modelDir + '/' + outFile + '.converted > tmp'
            os.system(cmd)
            scores.append(float(open('tmp').readline().strip().split(' ')[-1]))
    return sum(scores)/len(scores)

def getScore12(model):
    metricFile = model.replace('model.tar.gz', 'metrics.json')
    scores = ast.literal_eval(' '.join(open(metricFile).readlines()))
    return 100*(scores['best_validation_.run/entities12/acc'] + scores['best_validation_.run/relations12/acc'])/2

import csv
def getScore2(model):
    name = model.split('/')[-3]
    f = open('preds/' + name + '.csv', 'w')
    f.write('ID,Language,Setting,Label\n')
    writer = csv.writer(f)
    if ('emb' not in model and 'langs' not in model) or 'multitask' in model and '.task' in model:
        for line in open(model.replace('model.tar.gz', 'SEMEVAL2-A1.dev.out')):
            tok = line.strip().split('\t')
            writer.writerow(tok[:2] + ['one_shot'] + tok[-1:])
    else:
        for lang in ['en', 'pt']:
            for line in open(model.replace('model.tar.gz', 'SEMEVAL2-A1.' + lang + '.dev.out')):
                tok = line.strip().split('\t')
                writer.writerow(tok[:2] + ['one_shot'] + tok[-1:])
    f.close()
    cmd = 'python3 data/task2/SemEval_2022_Task2-idiomaticity/SubTaskA/SubTask1Evaluator.py preds/' + name + '.csv data/task2/SemEval_2022_Task2-idiomaticity/SubTaskA/Data/dev_gold.csv > tmp'
    # TODO call function directly?
    os.system(cmd)

    return float(open('tmp').readlines()[-1].strip().split('\t')[-1]) * 100
        
import task3

def getScore31(model):
    name = model.split('/')[-3]
    enGold = [int(x.strip().split('\t')[2]) for x in open('data/task3/1.dev.en.conll').readlines()]
    frGold = [int(x.strip().split('\t')[2]) for x in open('data/task3/1.dev.en.conll').readlines()]
    itGold = [int(x.strip().split('\t')[2]) for x in open('data/task3/1.dev.en.conll').readlines()]
    if ('emb' not in model and 'langs' not in model) or 'multitask' in model and '.task' in model:
        preds = [int(x.strip().split('\t')[2]) for x in open(model.replace('model.tar.gz', 'SEMEVAL3-1.dev.out')).readlines()]
        enPreds = preds[:1947]
        frPreds = preds[1947:3894]
        itPreds = preds[3894:]
        scores = [task3.compute_metrics_task1(enPreds, enGold)['F1macro'], task3.compute_metrics_task1(frPreds, frGold)['F1macro'], task3.compute_metrics_task1(itPreds, itGold)['F1macro']]
    else:
        scores= []
        for lang, gold in zip(['en', 'fr', 'it'], [enGold, frGold, itGold]):
            langScores = [int(x.strip().split('\t')[2]) for x in open(model.replace('model.tar.gz', 'SEMEVAL3-1.' + lang + '.dev.out')).readlines()]
            scores.append(task3.compute_metrics_task1(langScores, gold)['F1macro'])
    return 100* sum(scores)/len(scores)

from scipy.stats import pearsonr

def getScore32(model):
    name = model.split('/')[-3]
    enGold = [float(x.strip().split('\t')[2]) for x in open('data/task3/2.dev.en.conll').readlines()]
    frGold = [float(x.strip().split('\t')[2]) for x in open('data/task3/2.dev.fr.conll').readlines()]
    itGold = [float(x.strip().split('\t')[2]) for x in open('data/task3/2.dev.it.conll').readlines()]
    if ('emb' not in model and 'langs' not in model) or 'multitask' in model and '.task' in model:
        preds = [float(x.strip().split('\t')[2]) for x in open(model.replace('model.tar.gz', 'SEMEVAL3-2.dev.out')).readlines()]
        enPreds = preds[:262]
        frPreds = preds[262:524]
        itPreds = preds[524:]
        scores = [task3.compute_metrics_task2(enPreds, enGold)['rho'], task3.compute_metrics_task2(frPreds, frGold)['rho'], task3.compute_metrics_task2(itPreds, itGold)['rho']]
    else:
        scores= []
        for lang, gold in zip(['en', 'fr', 'it'], [enGold, frGold, itGold]):
            langScores = [float(x.strip().split('\t')[2]) for x in open(model.replace('model.tar.gz', 'SEMEVAL3-2.' + lang + '.dev.out')).readlines()]
            scores.append(task3.compute_metrics_task2(langScores, gold)['rho'])
    return 100* sum(scores)/len(scores)

def getScore41(model):
    os.system('mkdir -p preds/task41')
    os.system('mkdir -p preds/task41/res')
    os.system('mkdir -p preds/task41/ref')
    os.system('cut -f 6 data/task4/1.dev.conll > preds/task41/ref/task1.txt')
    os.system('cut -f 6 ' + model.replace('model.tar.gz', 'SEMEVAL4-1.dev.out') + ' > preds/task41/res/task1.txt')
    os.system('python3 data/task4/dontpatronizeme/semeval-2022/evaluation.py preds/task41/ preds/task41/')
    return float(open('preds/task41/scores.txt').readlines()[-1].strip().split(':')[1]) * 100

cats = ['Unbalanced_power_relations', 'Shallow_solution', 'Presupposition', 'Authority_voice', 'Metaphors', 'Compassion', 'The_poorer_the_merrier']
def convert42(inFile, outFile):
    f = open(outFile, 'w')
    for line in open(inFile):
        label = line.strip().split('\t')[8]
        outList = ['0'] * len(cats)
        labelIdx = cats.index(label)
        outList[labelIdx] = '1'
        f.write(','.join(outList) + '\n')
    f.close()

def getScore42(model):
    os.system('mkdir -p preds/task42')
    os.system('mkdir -p preds/task42/res')
    os.system('mkdir -p preds/task42/ref')
    convert42('data/task4/2.dev.conll', 'preds/task42/ref/task2.txt')
    convert42(model.replace('model.tar.gz', 'SEMEVAL4-2.dev.out'), 'preds/task42/res/task2.txt')
    os.system('sed -i "s;@@PADDING@@;Presupposition;g" ' + model.replace('model.tar.gz', 'SEMEVAL4-2.dev.out'))
    os.system('python3 data/task4/dontpatronizeme/semeval-2022/evaluation.py preds/task42/ preds/task42/')
    return float(open('preds/task42/scores.txt').readlines()[-1].strip().split(':')[1]) * 100

def f1(golds, preds):
    tp = 0
    fp = 0
    tn = 0
    fn = 0
    for gold, pred in zip(golds, preds):
        if gold ==1 :
            if pred ==1:
                tp += 1
            else:
                fn += 1
        else:
            if pred == 1:
                fp += 1
            else:
                tn += 1
    if tp+fp != 0:
        prec = tp/(tp+fp)
    else:
        prec = 0.0
    if tp+fn != 0:
        rec = tp/(tp+fn)
    else:
        rec = 0.0
    if prec +rec != 0:
        f1 = 2 * (prec*rec)/(prec+rec)
    else:
        f1 = 0.0
    return f1

def getScore6a(model):
    goldData = [int(x.strip().split('\t')[2]) for x in open('data/task6/1.dev.all.conll').readlines()]
    if ('emb' not in model and 'langs' not in model) or 'multitask' in model and '.task' in model:
        predData = [int(x.strip().split('\t')[2]) for x in open(model.replace('model.tar.gz', 'SEMEVAL6-a.dev.out')).readlines()]
    else:
        enData = [int(x.strip().split('\t')[2]) for x in open(model.replace('model.tar.gz', 'SEMEVAL6-a.en.dev.out')).readlines()]
        arData = [int(x.strip().split('\t')[2]) for x in open(model.replace('model.tar.gz', 'SEMEVAL6-a.ar.dev.out')).readlines()]
        predData = enData + arData
    return 100*f1(goldData, predData)

def getScore6b(model):
    goldData = [x.strip().split('\t') for x in open('data/task6/2.dev.en.conll').readlines()]
    predData = [x.strip().split('\t') for x in open(model.replace('model.tar.gz', 'SEMEVAL6-b.dev.out')).readlines()]
    f1irony = f1([int(x[5]) for x in goldData], [int(x[5]) for x in predData])
    f1satire = f1([int(x[6]) for x in goldData], [int(x[6]) for x in predData])
    f1understatement = f1([int(x[7]) for x in goldData], [int(x[7]) for x in predData])
    f1overstatement = f1([int(x[8]) for x in goldData], [int(x[8]) for x in predData])
    f1rhetorical = f1([int(x[9]) for x in goldData], [int(x[9]) for x in predData])
    scores = [f1irony, f1satire, f1understatement, f1overstatement, f1rhetorical]
    return 100*sum(scores)/len(scores) 

def getScore6c(model):
    goldData = [int(x.strip().split('\t')[2]) for x in open('data/task6/3.dev.all.conll').readlines()]
    if ('emb' not in model and 'langs' not in model) or 'multitask' in model and '.task' in model:
        predData = [int(x.strip().split('\t')[2]) for x in open(model.replace('model.tar.gz', 'SEMEVAL6-c.dev.out')).readlines()]
    else:
        enData = [int(x.strip().split('\t')[2]) for x in open(model.replace('model.tar.gz', 'SEMEVAL6-c.en.dev.out')).readlines()]
        arData = [int(x.strip().split('\t')[2]) for x in open(model.replace('model.tar.gz', 'SEMEVAL6-c.ar.dev.out')).readlines()]
        predData = enData + arData
    acc = sum([1 if x == y else 0 for x, y in zip(goldData, predData)])/len(goldData)
    return 100*acc

def getScore(name, taskName=''):
    model = myutils.getModel(name)
    if model == '':
        #print(name + '\\\\')
        return -1.0
    if 'task2'  in name or 'task2' in taskName:
        return getScore2(model)
    elif 'task3-1'  in name or 'task3-1' in taskName:
        return getScore31(model)
    elif 'task3-2'  in name or 'task3-2' in taskName:
        return getScore32(model)
    elif 'task4-1'  in name or 'task4-1' in taskName:
        return getScore41(model)
    elif 'task4-2'  in name or 'task4-2' in taskName:
        return getScore42(model)
    elif 'task6-a'  in name or 'task6-a' in taskName:
        return getScore6a(model)
    elif 'task6-b'  in name or 'task6-b' in taskName:
        return getScore6b(model)
    elif 'task6-c'  in name or 'task6-c' in taskName:
        return getScore6c(model)
    elif 'task10'  in name or 'task10' in taskName:
        if 'cross' in name:
            score, crossScore = getScore10(model.replace('-cross', ''))

            return crossScore
        else:
            #score, crossScore = getScore10(model)
            return getScore10(model)#score
    elif 'task11'  in name or 'task11' in taskName:
        return getScores11(model)
    elif 'task12' in name or 'task12' in taskName:
        return getScore12(model)
    else:
        metricFile = model.replace('model.tar.gz', 'metrics.json')
        scores = ast.literal_eval(' '.join(open(metricFile).readlines()))
        allScores = []
        for score in scores:
            if score.startswith('best_validation_.run') and score != 'best_validation_.run/.sum':
                allScores.append(scores[score])
    return 100*sum(allScores)/len(allScores)

