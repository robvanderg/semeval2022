import myutils
import os
import csv

os.system('mkdir -p preds/task4')


os.system('cat data/task4/dontpatronizeme/semeval-2022/TEST/task4_test.tsv | cut -f 1,2,5 > tmp')
os.system('paste tmp tmp | cut -f 1,2,3,4,6 > data/task4/dontpatronizeme/semeval-2022/TEST/task4_test.conll')
testPath = 'data/task4/dontpatronizeme/semeval-2022/TEST/task4_test.conll'

# predict
for modelName in ['multitask.mbert.task4-1', 'task4-1']:
    model = myutils.getModel(modelName)
    if model == '':
        continue
    cmd = 'python3 predict.py ' + model[4:] + ' ../' + testPath + ' ../' + testPath + '-' + modelName
    print(cmd)

for modelName in ['multitask.rembert.emb.task4-2', 'task4-2']:
    model = myutils.getModel(modelName)
    if model == '':
        continue
    cmd = 'python3 predict.py ' + model[4:] + ' ../' + testPath + ' ../' + testPath + '-' + modelName
    print(cmd)

