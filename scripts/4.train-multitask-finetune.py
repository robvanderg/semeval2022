import os
import myutils

configs1 = ['task2-a1-emb.json', 'task3-1-emb.json', 'task3-2-emb.json', 'task4-1-emb.json', 'task4-2-emb.json', 'task6-a-emb.json', 'task6-b-emb.json', 'task6-c-emb.json',  'task10-optionB-emb.json', 'task11-emb.json', 'task12-optionB-emb.json']
configs1 = ['../configs/' + x for x in configs1]
configs2 = [x.replace('-emb', '') for x in configs1]



prevModels = []
for embed, params in zip(['mbert', 'xlmr', 'rembert', 'twitter', 'deberta', 'mluke'], ['params.smoothSampling', 'params-xlmr-smooth', 'params-rembert-smooth', 'params-twitter-smooth', 'params-deberta-smooth', 'params-mluke-smooth']):
    for setting in ['.emb', '']:
        preName = 'multitask.' + embed + setting
        model = myutils.getModel(preName).replace('model.tar.gz', '')[4:]
        if model == '':
            print("NOT FOUND", preName)
            continue
        for config in configs2:
            name = preName + '.' + config[config.rfind('/')+1:-5]
            if myutils.getModel(name) == '':
                cmd = 'python3 train.py --finetune ' + model + ' --parameters_config ../configs/' + params + '.json --dataset_config ' + config + ' --name ' + name
                print(cmd)

