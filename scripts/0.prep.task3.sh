cd data
mkdir task3
cd task3 
git clone https://github.com/shammur/SemEval2022Task3.git
cd SemEval2022Task3
git reset --hard a934bcee4e2b91310ed86a51a95176432d4d7f5b
cd ../../../

# subtask1
sed '1d' data/task3/SemEval2022Task3/data/train/train_subtask-1/en/En-Subtask1-fold_0.tsv > data/task3/1.train.en.conll
sed '1d' data/task3/SemEval2022Task3/data/train/train_subtask-1/en/En-Subtask1-fold_1.tsv >> data/task3/1.train.en.conll 
sed '1d' data/task3/SemEval2022Task3/data/train/train_subtask-1/fr/Fr-Subtask1-fold_0.tsv > data/task3/1.train.fr.conll
sed '1d' data/task3/SemEval2022Task3/data/train/train_subtask-1/fr/Fr-Subtask1-fold_1.tsv >> data/task3/1.train.fr.conll
sed '1d' data/task3/SemEval2022Task3/data/train/train_subtask-1/it/It-Subtask1-fold_0.tsv > data/task3/1.train.it.conll
sed '1d' data/task3/SemEval2022Task3/data/train/train_subtask-1/it/It-Subtask1-fold_1.tsv >> data/task3/1.train.it.conll
cat data/task3/1.train.en.conll data/task3/1.train.fr.conll data/task3/1.train.it.conll > data/task3/1.train.all.conll

sed '1d' data/task3/SemEval2022Task3/data/train/train_subtask-1/en/En-Subtask1-fold_2.tsv > data/task3/1.dev.en.conll
sed '1d' data/task3/SemEval2022Task3/data/train/train_subtask-1/fr/Fr-Subtask1-fold_2.tsv > data/task3/1.dev.fr.conll
sed '1d' data/task3/SemEval2022Task3/data/train/train_subtask-1/it/It-Subtask1-fold_2.tsv > data/task3/1.dev.it.conll
cat data/task3/1.dev.en.conll data/task3/1.dev.fr.conll data/task3/1.dev.it.conll > data/task3/1.dev.all.conll

# subtask2
sed '1d' data/task3/SemEval2022Task3/data/train/train_subtask-2/en/En-Subtask2-fold_0.tsv > data/task3/2.train.en.conll
sed '1d' data/task3/SemEval2022Task3/data/train/train_subtask-2/fr/Fr-Subtask2-fold_0.tsv > data/task3/2.train.fr.conll
sed '1d' data/task3/SemEval2022Task3/data/train/train_subtask-2/it/It-Subtask2-fold_0.tsv > data/task3/2.train.it.conll
cat data/task3/2.train.en.conll data/task3/2.train.fr.conll data/task3/2.train.it.conll > data/task3/2.train.all.conll

sed '1d' data/task3/SemEval2022Task3/data/train/train_subtask-2/en/En-Subtask2-fold_1.tsv > data/task3/2.dev.en.conll
sed '1d' data/task3/SemEval2022Task3/data/train/train_subtask-2/fr/Fr-Subtask2-fold_1.tsv > data/task3/2.dev.fr.conll
sed '1d' data/task3/SemEval2022Task3/data/train/train_subtask-2/it/It-Subtask2-fold_1.tsv > data/task3/2.dev.it.conll
cat data/task3/2.dev.en.conll data/task3/2.dev.fr.conll data/task3/2.dev.it.conll > data/task3/2.dev.all.conll


