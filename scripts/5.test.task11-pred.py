import myutils
import os

def cleanPath(path):
    data = []
    curSent = []
    for line in open(path):
        if len(line) < 2:
            data.append(curSent)
            curSent = []
        else:
            curSent.append(line)
    outFile = open(path, 'w')
    for sent in data:
        if sent[1].startswith('#	_'):
            sent = [sent[0]] + sent[2:]
        for line in sent:
            outFile.write(line)
        outFile.write('\n')
    outFile.close()

os.system('mkdir -p test/task11')
os.system('mkdir -p test/task11/single/')
os.system('mkdir -p test/task11/multi/')
os.system('sed -i "s; ;	;g" data/task11/test/*/*conll') 
os.system('sed -i "s;#	id	;# id ;g" data/task11/test/*/*conll')
def getTest(path):
    for testFile in os.listdir(path):
        if 'test' in testFile:
            return path + '/' + testFile

for modelName in ['task11', 'multitask.rembert.emb.task11']:
    model = myutils.getModel(modelName)
    if model == '':
        continue
    for dataDir in os.listdir('data/task11/test/'):
        if os.path.isdir('data/task11/test/' + dataDir):
            testFile = getTest('data/task11/test/' + dataDir)
            #cleanPath(testFile)
            lang = testFile.split('/')[-1].split('_')[0]
            outPath = 'test/task11/' + lang + '.' + modelName
            cmd = 'python3 predict.py ' + model[4:] + ' ../' + testFile + ' ../' + outPath
            print(cmd)

