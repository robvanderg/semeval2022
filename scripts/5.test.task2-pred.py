import myutils
import os
import csv

os.system('mkdir -p preds/task2')

# convert test data
def csv2data(path, delimiter=',' ) :
    header = None
    data   = list()
    with open(path, encoding='utf-8') as csvfile:
      reader = csv.reader( csvfile, delimiter=delimiter )
      for row in reader :
          if header is None :
              header = row
              continue
          data.append( row )
    return data

def csv2conll(path):
    data = csv2data(path)
    outPath = open(path + '.conll', 'w')
    for item in data:
        item = [x.replace('\t', ' ').replace('\n', ' ') for x in item]
        #del item[3]
        outPath.write('\t'.join(item) + '\n')
    outPath.close()

testPath = 'data/task2/SemEval_2022_Task2-idiomaticity/SubTaskA/TestData/test.csv'
csv2conll(testPath)

# predict
for modelName in ['multitask.rembert.emb.task2-a1', 'task2-a1']:
    model = myutils.getModel(modelName)
    if model == '':
        continue
    cmd = 'python3 predict.py ' + model[4:] + ' ../' + testPath + '.conll ../' + testPath + '-' + modelName
    print(cmd)


