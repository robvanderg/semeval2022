import csv
import os

def csv2data(path, delimiter=',' ) :
    header = None
    data   = list()
    with open(path, encoding='utf-8') as csvfile:
      reader = csv.reader( csvfile, delimiter=delimiter )
      for row in reader :
          if header is None :
              header = row
              continue
          data.append( row )
    return data

def csv2conll(path):
    data = csv2data(path)
    outPath = open(path + '.conll', 'w')
    for item in data:
        item = [x.replace('\t', ' ').replace('\n', ' ') for x in item]
        del item[3]
        outPath.write('\t'.join(item) + '\n')
    outPath.close()

def comb(path1, path2, outPath):
    data1 = csv2data(path1)
    data2 = csv2data(path2)
    outFile = open(outPath, 'w')
    for item1, item2 in zip(data1, data2):
        item1 = [x.replace('\t', ' ').replace('\n', ' ') for x in item1]
        outFile.write('\t'.join(item1 + [item2[-1]]) + '\n')
    outFile.close()

def sep(path):
    langs = {}
    for line in open(path):
        tok = line.strip().split('\t')
        if tok[1] not in langs:
            langs[tok[1]] = []
        langs[tok[1]].append(tok)
    for lang in langs:
        outFile = open(path.replace('all', lang.lower()), 'w')
        for line in langs[lang]:
            outFile.write('\t'.join(line) + '\n')
        outFile.close()

csv2conll('data/task2/SemEval_2022_Task2-idiomaticity/SubTaskA/Data/train_one_shot.csv')
os.system('cp data/task2/SemEval_2022_Task2-idiomaticity/SubTaskA/Data/train_one_shot.csv.conll data/task2/train.all.conll')

comb('data/task2/SemEval_2022_Task2-idiomaticity/SubTaskA/Data/dev.csv', 'data/task2/SemEval_2022_Task2-idiomaticity/SubTaskA/Data/dev_gold.csv', 'data/task2/SemEval_2022_Task2-idiomaticity/SubTaskA/Data/dev.csv.conll')
os.system('cp data/task2/SemEval_2022_Task2-idiomaticity/SubTaskA/Data/dev.csv.conll data/task2/dev.all.conll')

sep('data/task2/train.all.conll')
sep('data/task2/dev.all.conll')

