import myutils

for config in ['task3-1', 'task3-2']:
    langConfig, langEmbConfig, embConfig = myutils.genConfigs(config, ['en', 'fr', 'it'])

    for dataConfig in ['configs/' + config + '.json', langConfig, langEmbConfig]:
        myutils.train(dataConfig)

